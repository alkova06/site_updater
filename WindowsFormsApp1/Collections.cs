﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp1
{
    class Collections
    {
        public List<Tuple<float, string>> untappdCollection = new List<Tuple<float, string>>();
        public List<Tuple<int, string>> IBUCollection = new List<Tuple<int, string>>();
        public List<Tuple<float, string>> ABVCollection = new List<Tuple<float, string>>();

        public string[] RedAndAmberStyles = { "Altbier", "Red ", "Brown " };//червоне та бурштинове
        public string[] BrownAndDarkStyles = { "Dunkel", "Dark ", "Brown ", "Schwarzbier" };//червоне та бурштинове
        public string[] IPAStyles = { "IPA " };//IPA
        public string[] PaleAndBlondStyles = { "Pale Ale", "Blonde", "Cream ", "Bitter", "Mild" };//IPA
        public string[] BelgianLikeStyles = { "Belgian " };//
        public string[] StoutAndPorterStyles = { "Stout", "Porter" };
        public string[] SourWildFarmhouseStyles = { "Sour ", "Wild", "Farmhouse" };
        public string[] PilsnerLagerStyles = { "Pilsner", "Lager" };
        public string[] WithAdditionalsStyles = { "Fruit", "Chilli", "Honey", "Root", "Grape", "Ginger", "Winter", "Pumpkin", "Coffe", "Radler", "Spiced", "Milk", "Pastry", "Oyster", "Smoked", "rauch" };
        public string[] WheatWitStyles = { "Wheat", "weizen" };
        public string[] StrongStyles = { "wine", "bock", "Double", "Tripel", "Imperial", "strong", "quadrupel", "old", "Scottish Ale", "freeze" };
        public string[] CiderMeadStyles = { "Cider", "Mead" };
        public string[] NonAlcoholic = { "Non-Alcoholic" };

        public Collections()
        {
            untappdCollection.Add(new Tuple<float, string>(0.0f, "Новинка (без оцінки)"));
            untappdCollection.Add(new Tuple<float, string>(3.1f, "Ммм... (менше ніж 3.1)"));
            untappdCollection.Add(new Tuple<float, string>(3.3f, "Для чекіна (3.1-3.3)"));
            untappdCollection.Add(new Tuple<float, string>(3.5f, "Добре (3.3-3.5)"));
            untappdCollection.Add(new Tuple<float, string>(3.7f, "Дуже добре (3.5-3.7)"));
            untappdCollection.Add(new Tuple<float, string>(3.9f, "Файно (3.7-3.9)"));
            untappdCollection.Add(new Tuple<float, string>(4.1f, "Чудовий вибір (3.9-4.1)"));
            untappdCollection.Add(new Tuple<float, string>(4.3f, "Топчик (4.1-4.3)"));
            untappdCollection.Add(new Tuple<float, string>(4.5f, "Нектар богів (4.3-4.5)"));
            untappdCollection.Add(new Tuple<float, string>(5.0f, "Нелегально (4.5 та більше)"));

            IBUCollection.Add(new Tuple<int, string>(0, "N/A IBU"));
            IBUCollection.Add(new Tuple<int, string>(19, "0-19 IBU"));
            IBUCollection.Add(new Tuple<int, string>(39, "20-39 IBU"));
            IBUCollection.Add(new Tuple<int, string>(59, "40-59 IBU"));
            IBUCollection.Add(new Tuple<int, string>(79, "60-79 IBU"));
            IBUCollection.Add(new Tuple<int, string>(99, "80-99 IBU"));
            IBUCollection.Add(new Tuple<int, string>(9999999, "Геноцид рецепторів"));

            ABVCollection.Add(new Tuple<float, string>(0.5f, "Безалкогольне (до 0.5% ABV)"));
            ABVCollection.Add(new Tuple<float, string>(3.9f, "Пиво до сніданку (0.5%-3.9% ABV)"));
            ABVCollection.Add(new Tuple<float, string>(5.4f, "Пиво до обіду (4.0%-5.4% ABV)"));
            ABVCollection.Add(new Tuple<float, string>(7.4f, "Пиво для п'ятниці (5.5%-7.4% ABV)"));
            ABVCollection.Add(new Tuple<float, string>(9.9f, "Подумати про життя (7.5%-9.9% ABV)"));
            ABVCollection.Add(new Tuple<float, string>(99.0f, "Удар по печінці (10% ABV та більше)"));

        }
    }
}
