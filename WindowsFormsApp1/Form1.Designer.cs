﻿namespace WindowsFormsApp1
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonOpenPosterInventory = new System.Windows.Forms.Button();
            this.checkedListBox1 = new System.Windows.Forms.CheckedListBox();
            this.buttonProcessPosterInventory = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.buttonOpenWIXFile = new System.Windows.Forms.Button();
            this.dataGridViewWIX = new System.Windows.Forms.DataGridView();
            this.dataGridViewPoster = new System.Windows.Forms.DataGridView();
            this.label2 = new System.Windows.Forms.Label();
            this.buttonOpenPosterPrice = new System.Windows.Forms.Button();
            this.buttonProcessPosterPrice = new System.Windows.Forms.Button();
            this.checkedListBox2 = new System.Windows.Forms.CheckedListBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.buttonAddUpdateWIX = new System.Windows.Forms.Button();
            this.counterLabel = new System.Windows.Forms.Label();
            this.buttonUpdateWIXID = new System.Windows.Forms.Button();
            this.richTextBoxAdded = new System.Windows.Forms.RichTextBox();
            this.richTextBoxUpdated = new System.Windows.Forms.RichTextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.buttonSaveWIX = new System.Windows.Forms.Button();
            this.buttonUpdateWIXInventory = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.UpdatePhotosButton = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewWIX)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewPoster)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // buttonOpenPosterInventory
            // 
            this.buttonOpenPosterInventory.Location = new System.Drawing.Point(6, 19);
            this.buttonOpenPosterInventory.Name = "buttonOpenPosterInventory";
            this.buttonOpenPosterInventory.Size = new System.Drawing.Size(120, 39);
            this.buttonOpenPosterInventory.TabIndex = 0;
            this.buttonOpenPosterInventory.Text = "Open";
            this.buttonOpenPosterInventory.UseVisualStyleBackColor = true;
            this.buttonOpenPosterInventory.Click += new System.EventHandler(this.buttonOpenPosterInventory_Click);
            // 
            // checkedListBox1
            // 
            this.checkedListBox1.FormattingEnabled = true;
            this.checkedListBox1.Location = new System.Drawing.Point(6, 91);
            this.checkedListBox1.Name = "checkedListBox1";
            this.checkedListBox1.Size = new System.Drawing.Size(120, 139);
            this.checkedListBox1.TabIndex = 3;
            // 
            // buttonProcessPosterInventory
            // 
            this.buttonProcessPosterInventory.Location = new System.Drawing.Point(6, 236);
            this.buttonProcessPosterInventory.Name = "buttonProcessPosterInventory";
            this.buttonProcessPosterInventory.Size = new System.Drawing.Size(120, 39);
            this.buttonProcessPosterInventory.TabIndex = 4;
            this.buttonProcessPosterInventory.Text = "Process";
            this.buttonProcessPosterInventory.UseVisualStyleBackColor = true;
            this.buttonProcessPosterInventory.Click += new System.EventHandler(this.buttonProcessPosterInventory_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.buttonOpenPosterInventory);
            this.groupBox1.Controls.Add(this.buttonProcessPosterInventory);
            this.groupBox1.Controls.Add(this.checkedListBox1);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(164, 286);
            this.groupBox1.TabIndex = 5;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Poster->Wix (Залишки)";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 75);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(159, 13);
            this.label1.TabIndex = 5;
            this.label1.Text = "Обрати Категорія та Кількість";
            // 
            // buttonOpenWIXFile
            // 
            this.buttonOpenWIXFile.Location = new System.Drawing.Point(368, 31);
            this.buttonOpenWIXFile.Name = "buttonOpenWIXFile";
            this.buttonOpenWIXFile.Size = new System.Drawing.Size(120, 39);
            this.buttonOpenWIXFile.TabIndex = 5;
            this.buttonOpenWIXFile.Text = "Open";
            this.buttonOpenWIXFile.UseVisualStyleBackColor = true;
            this.buttonOpenWIXFile.Click += new System.EventHandler(this.buttonOpenWIXFile_Click);
            // 
            // dataGridViewWIX
            // 
            this.dataGridViewWIX.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewWIX.Location = new System.Drawing.Point(502, 30);
            this.dataGridViewWIX.Name = "dataGridViewWIX";
            this.dataGridViewWIX.Size = new System.Drawing.Size(718, 268);
            this.dataGridViewWIX.TabIndex = 6;
            // 
            // dataGridViewPoster
            // 
            this.dataGridViewPoster.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewPoster.Location = new System.Drawing.Point(12, 335);
            this.dataGridViewPoster.Name = "dataGridViewPoster";
            this.dataGridViewPoster.Size = new System.Drawing.Size(698, 285);
            this.dataGridViewPoster.TabIndex = 7;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 74);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(175, 13);
            this.label2.TabIndex = 9;
            this.label2.Text = "Оберіть Назва, Категорія та Ціна";
            // 
            // buttonOpenPosterPrice
            // 
            this.buttonOpenPosterPrice.Location = new System.Drawing.Point(6, 18);
            this.buttonOpenPosterPrice.Name = "buttonOpenPosterPrice";
            this.buttonOpenPosterPrice.Size = new System.Drawing.Size(120, 39);
            this.buttonOpenPosterPrice.TabIndex = 6;
            this.buttonOpenPosterPrice.Text = "Open";
            this.buttonOpenPosterPrice.UseVisualStyleBackColor = true;
            this.buttonOpenPosterPrice.Click += new System.EventHandler(this.buttonOpenPosterPrice_Click);
            // 
            // buttonProcessPosterPrice
            // 
            this.buttonProcessPosterPrice.Location = new System.Drawing.Point(6, 235);
            this.buttonProcessPosterPrice.Name = "buttonProcessPosterPrice";
            this.buttonProcessPosterPrice.Size = new System.Drawing.Size(120, 39);
            this.buttonProcessPosterPrice.TabIndex = 8;
            this.buttonProcessPosterPrice.Text = "Process";
            this.buttonProcessPosterPrice.UseVisualStyleBackColor = true;
            this.buttonProcessPosterPrice.Click += new System.EventHandler(this.buttonProcessPosterPrice_Click);
            // 
            // checkedListBox2
            // 
            this.checkedListBox2.FormattingEnabled = true;
            this.checkedListBox2.Location = new System.Drawing.Point(6, 90);
            this.checkedListBox2.Name = "checkedListBox2";
            this.checkedListBox2.Size = new System.Drawing.Size(120, 139);
            this.checkedListBox2.TabIndex = 7;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.checkedListBox2);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.buttonProcessPosterPrice);
            this.groupBox2.Controls.Add(this.buttonOpenPosterPrice);
            this.groupBox2.Location = new System.Drawing.Point(182, 12);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(180, 286);
            this.groupBox2.TabIndex = 6;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Poster->Wix (Ціна)";
            // 
            // buttonAddUpdateWIX
            // 
            this.buttonAddUpdateWIX.Location = new System.Drawing.Point(368, 118);
            this.buttonAddUpdateWIX.Name = "buttonAddUpdateWIX";
            this.buttonAddUpdateWIX.Size = new System.Drawing.Size(120, 39);
            this.buttonAddUpdateWIX.TabIndex = 8;
            this.buttonAddUpdateWIX.Text = "Add/Update Products";
            this.buttonAddUpdateWIX.UseVisualStyleBackColor = true;
            this.buttonAddUpdateWIX.Click += new System.EventHandler(this.buttonAddUpdateWIX_Click);
            // 
            // counterLabel
            // 
            this.counterLabel.AutoSize = true;
            this.counterLabel.Location = new System.Drawing.Point(18, 301);
            this.counterLabel.Name = "counterLabel";
            this.counterLabel.Size = new System.Drawing.Size(44, 13);
            this.counterLabel.TabIndex = 10;
            this.counterLabel.Text = "Counter";
            // 
            // buttonUpdateWIXID
            // 
            this.buttonUpdateWIXID.Location = new System.Drawing.Point(368, 73);
            this.buttonUpdateWIXID.Name = "buttonUpdateWIXID";
            this.buttonUpdateWIXID.Size = new System.Drawing.Size(120, 39);
            this.buttonUpdateWIXID.TabIndex = 11;
            this.buttonUpdateWIXID.Text = "Update ID";
            this.buttonUpdateWIXID.UseVisualStyleBackColor = true;
            this.buttonUpdateWIXID.Click += new System.EventHandler(this.buttonUpdateWIXID_Click);
            // 
            // richTextBoxAdded
            // 
            this.richTextBoxAdded.Location = new System.Drawing.Point(716, 335);
            this.richTextBoxAdded.Name = "richTextBoxAdded";
            this.richTextBoxAdded.Size = new System.Drawing.Size(250, 285);
            this.richTextBoxAdded.TabIndex = 12;
            this.richTextBoxAdded.Text = "";
            // 
            // richTextBoxUpdated
            // 
            this.richTextBoxUpdated.Location = new System.Drawing.Point(972, 335);
            this.richTextBoxUpdated.Name = "richTextBoxUpdated";
            this.richTextBoxUpdated.Size = new System.Drawing.Size(250, 285);
            this.richTextBoxUpdated.TabIndex = 13;
            this.richTextBoxUpdated.Text = "";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(713, 319);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(38, 13);
            this.label3.TabIndex = 14;
            this.label3.Text = "Added";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(969, 319);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(48, 13);
            this.label4.TabIndex = 15;
            this.label4.Text = "Updated";
            // 
            // buttonSaveWIX
            // 
            this.buttonSaveWIX.Location = new System.Drawing.Point(368, 259);
            this.buttonSaveWIX.Name = "buttonSaveWIX";
            this.buttonSaveWIX.Size = new System.Drawing.Size(120, 39);
            this.buttonSaveWIX.TabIndex = 16;
            this.buttonSaveWIX.Text = "Save";
            this.buttonSaveWIX.UseVisualStyleBackColor = true;
            this.buttonSaveWIX.Click += new System.EventHandler(this.buttonSaveWIX_Click);
            // 
            // buttonUpdateWIXInventory
            // 
            this.buttonUpdateWIXInventory.Location = new System.Drawing.Point(368, 188);
            this.buttonUpdateWIXInventory.Name = "buttonUpdateWIXInventory";
            this.buttonUpdateWIXInventory.Size = new System.Drawing.Size(120, 39);
            this.buttonUpdateWIXInventory.TabIndex = 17;
            this.buttonUpdateWIXInventory.Text = "Update Count";
            this.buttonUpdateWIXInventory.UseVisualStyleBackColor = true;
            this.buttonUpdateWIXInventory.Click += new System.EventHandler(this.buttonUpdateWIXInventory_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(1226, 30);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(81, 39);
            this.button1.TabIndex = 18;
            this.button1.Text = "Generate Collections";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // UpdatePhotosButton
            // 
            this.UpdatePhotosButton.Location = new System.Drawing.Point(1226, 86);
            this.UpdatePhotosButton.Name = "UpdatePhotosButton";
            this.UpdatePhotosButton.Size = new System.Drawing.Size(81, 39);
            this.UpdatePhotosButton.TabIndex = 19;
            this.UpdatePhotosButton.Text = "Update Photos";
            this.UpdatePhotosButton.UseVisualStyleBackColor = true;
            this.UpdatePhotosButton.Click += new System.EventHandler(this.UpdatePhotosButton_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1352, 632);
            this.Controls.Add(this.UpdatePhotosButton);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.buttonUpdateWIXInventory);
            this.Controls.Add(this.buttonSaveWIX);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.richTextBoxUpdated);
            this.Controls.Add(this.richTextBoxAdded);
            this.Controls.Add(this.buttonUpdateWIXID);
            this.Controls.Add(this.counterLabel);
            this.Controls.Add(this.buttonAddUpdateWIX);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.dataGridViewPoster);
            this.Controls.Add(this.dataGridViewWIX);
            this.Controls.Add(this.buttonOpenWIXFile);
            this.Controls.Add(this.groupBox1);
            this.Name = "Form1";
            this.Text = "Poster Wix Converter";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewWIX)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewPoster)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttonOpenPosterInventory;
        private System.Windows.Forms.CheckedListBox checkedListBox1;
        private System.Windows.Forms.Button buttonProcessPosterInventory;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button buttonOpenWIXFile;
        private System.Windows.Forms.DataGridView dataGridViewWIX;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridView dataGridViewPoster;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button buttonOpenPosterPrice;
        private System.Windows.Forms.Button buttonProcessPosterPrice;
        private System.Windows.Forms.CheckedListBox checkedListBox2;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button buttonAddUpdateWIX;
        private System.Windows.Forms.Label counterLabel;
        private System.Windows.Forms.Button buttonUpdateWIXID;
        private System.Windows.Forms.RichTextBox richTextBoxAdded;
        private System.Windows.Forms.RichTextBox richTextBoxUpdated;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button buttonSaveWIX;
        private System.Windows.Forms.Button buttonUpdateWIXInventory;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button UpdatePhotosButton;
    }
}

