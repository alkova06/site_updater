﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Security.Cryptography;
using System.Text;
using System.Windows.Forms;
using Excel = Microsoft.Office.Interop.Excel;

namespace WindowsFormsApp1
{
    public struct PosterInfo
    {
        public string Name;
        public string Type;
        public string Volume;
        public string Container;
        public int Count;
        public string Brewery;
        public float Price;
    }

    public partial class Form1 : Form
    {
        DataTable wixDataTable = new DataTable();
        DataTable posterDataTable = new DataTable();

        Excel.Worksheet workSheet;
        Excel.Application workExcel;
        Excel.Workbook workBook;

        string[] ExcludedCategories = { "Пиво Крани", "Крани", "Закуски", "Мерч", "Головний екран", "Безалкогольні напої" };
        
        #region Constructor & Destructor
        public Form1()
        {
            InitializeComponent();

            posterDataTable.Columns.Add(new DataColumn("Назва"));
            posterDataTable.Columns.Add(new DataColumn("Броварня"));
            posterDataTable.Columns.Add(new DataColumn("Контейнер"));
            posterDataTable.Columns.Add(new DataColumn("Кількість"));
            posterDataTable.Columns.Add(new DataColumn("Тип"));
            posterDataTable.Columns.Add(new DataColumn("Об'єм"));
            posterDataTable.Columns.Add(new DataColumn("Ціна"));

            dataGridViewPoster.DataSource = posterDataTable;

            workExcel = new Excel.Application();
        }

        ~Form1()
        {
            if (workBook != null)
            {
                workBook.Close(false, Type.Missing, Type.Missing);
            }

            if (workExcel != null)
            {
                workExcel.Quit();
            }
        }
        #endregion
        #region Open Poster Inventory Count
        private void buttonOpenPosterInventory_Click(object sender, EventArgs e)
        {
            int firstColumn;
            int firstRow;
            if (!LoadXLSFile(out firstColumn, out firstRow))
            {
                return;
            }

            checkedListBox1.Items.Clear();
            checkedListBox2.Items.Clear();
            Excel.Range lastCell = workSheet.Cells.SpecialCells(Excel.XlCellType.xlCellTypeLastCell);
            for (int j = firstColumn - 1; j < lastCell.Column; j++)
            {
                checkedListBox1.Items.Add(workSheet.Cells[firstRow, j + 1].Text.ToString(), false);
            }

            GC.Collect();
        }
        #endregion
        #region Open Wix file
        private void buttonOpenWIXFile_Click(object sender, EventArgs e)
        {            
            wixDataTable = ParseCSV(OpenCSVFile());

            if (wixDataTable.Rows.Count > 0)
            {
                dataGridViewWIX.DataSource = wixDataTable;
            }

            GC.Collect();
        }
        #endregion
        #region Generate Data from poster inventory
        private void buttonProcessPosterInventory_Click(object sender, EventArgs e)
        {
            posterDataTable.Rows.Clear();

            Excel.Range lastCell = workSheet.Cells.SpecialCells(Excel.XlCellType.xlCellTypeLastCell);
            int lastColumn = (int)lastCell.Column;
            int lastRow = (int)lastCell.Row;

            int firstColum = -1;
            int firstRow = -1;

            for (int i = 0; i < lastCell.Row; i++)
            {
                for (int j = 0; j < lastCell.Column; j++)
                {
                    if (workSheet.Cells[i + 1, j + 1].Text.ToString() != "")
                    {
                        firstRow = i + 1;
                        firstColum = j + 1;
                        break;
                    }
                }

                if (firstRow != -1)
                {
                    break;
                }
            }

            int breweryCollum = -1;
            int countCollum = -1;
            if (checkedListBox1.CheckedIndices.Count > 1)
            {
                breweryCollum = checkedListBox1.CheckedIndices[0];
                countCollum = checkedListBox1.CheckedIndices[1];
            }

            for (int i = firstRow; i < lastRow; i++)
            {
                counterLabel.Text = i.ToString() + "/" + (lastRow - 1).ToString();

                if (breweryCollum > -1)
                {
                    bool skipProduct = false;
                    for (int j = 0; j < ExcludedCategories.Length; j++)
                    {
                        if (workSheet.Cells[i + 1, firstColum + breweryCollum].Text.ToString() == ExcludedCategories[j])
                        {
                            skipProduct = true;
                            break;
                        }
                    }

                    if (skipProduct)
                    {
                        continue;
                    }

                    PosterInfo posterInfo = ParsePosterName(workSheet.Cells[i + 1, firstColum].Text.ToString(), "");
                    posterInfo.Brewery = workSheet.Cells[i + 1, firstColum + breweryCollum].Text.ToString();
                    posterInfo.Count = (int)float.Parse(workSheet.Cells[i + 1, firstColum + countCollum].Text.ToString());

                    DataRow dr = posterDataTable.NewRow();
                    dr[0] = posterInfo.Name;
                    dr[1] = posterInfo.Brewery;
                    dr[2] = posterInfo.Container;
                    dr[3] = posterInfo.Count;
                    dr[4] = posterInfo.Type;
                    dr[5] = posterInfo.Volume;
                    posterDataTable.Rows.Add(dr);
                }
            }
            GC.Collect();
        }
        #endregion
        #region Open Poster Price file
        private void buttonOpenPosterPrice_Click(object sender, EventArgs e)
        {
            int firstColumn;
            int firstRow;
            if (!LoadXLSFile(out firstColumn, out firstRow))
            {
                return;
            }

            Excel.Range lastCell = workSheet.Cells.SpecialCells(Excel.XlCellType.xlCellTypeLastCell);

            checkedListBox2.Items.Clear();
            for (int j = firstColumn - 1; j < lastCell.Column; j++)
            {
                checkedListBox2.Items.Add(workSheet.Cells[firstRow, j + 1].Text.ToString(), false);
            }

            checkedListBox1.Items.Clear();
            GC.Collect();
        }
        #endregion
        #region Add prices
        private void buttonProcessPosterPrice_Click(object sender, EventArgs e)
        {
            Excel.Range lastCell = workSheet.Cells.SpecialCells(Excel.XlCellType.xlCellTypeLastCell);
            int lastColumn = (int)lastCell.Column;
            int lastRow = (int)lastCell.Row;

            int firstColum = -1;
            int firstRow = -1;

            for (int i = 0; i < lastCell.Row; i++)
            {
                for (int j = 0; j < lastCell.Column; j++)
                {
                    if (workSheet.Cells[i + 1, j + 1].Text.ToString() != "")
                    {
                        firstRow = i + 1;
                        firstColum = j + 1;
                        break;
                    }
                }

                if (firstRow != -1)
                {
                    break;
                }
            }

            int titleCollum = -1;
            int categoryCollum = -1;
            int priceCollum = -1;
            if (checkedListBox2.CheckedIndices.Count > 2)
            {
                titleCollum = checkedListBox2.CheckedIndices[0];
                categoryCollum = checkedListBox2.CheckedIndices[1];
                priceCollum = checkedListBox2.CheckedIndices[2];
            }

            List<Tuple<string, string>> NamePrices = new List<Tuple<string, string>>();

            for (int i = firstRow; i < lastCell.Row; i++)
            {
                counterLabel.Text = i.ToString() + "/" + lastCell.Row;

                bool skipProduct = false;
                for (int j = 0; j < ExcludedCategories.Length; j++)
                {
                    if (workSheet.Cells[i + 1, firstColum + categoryCollum].Text.ToString() == ExcludedCategories[j])
                    {
                        skipProduct = true;
                        break;
                    }
                }

                if (skipProduct)
                {
                    continue;
                }
                NamePrices.Add(new Tuple<string, string>(workSheet.Cells[i + 1, firstColum + titleCollum].Text.ToString(), workSheet.Cells[i + 1, firstColum + priceCollum].Text.ToString()));
            }

            for (int j = 0; j < NamePrices.Count; j++)
            {
                string title = NamePrices[j].Item1;
                title = title.Replace(".", ",");
                title = title.Split(' ')[title.Split(' ').Length - 1];
                float price = 0;
                if (!float.TryParse(title, out price) || price <= 0)
                {
                    richTextBoxUpdated.Text += NamePrices[j].Item1 + "\n";
                }
            }

            for (int i = 0; i < posterDataTable.Rows.Count; i++)
            {
                bool isFound = false;
                for (int j = 0; j < NamePrices.Count; j++)
                {
                    counterLabel.Text = (i * NamePrices.Count + j).ToString() + "/" + (NamePrices.Count * posterDataTable.Rows.Count).ToString();
                    
                    string titleTmp = NamePrices[j].Item1.ToLower().Replace(',', '.');

                    if (NamePrices[j].Item1.ToLower().Replace(',', '.').Replace("\"", "").Contains(posterDataTable.Rows[i][0].ToString().ToLower().Replace("\"", "").Split(new string[] { "банка" }, StringSplitOptions.None)[0].Replace(',', '.')))
                    {
                        posterDataTable.Rows[i]["Ціна"] = NamePrices[j].Item2;
                        NamePrices.RemoveAt(j);
                        isFound = true;
                        break;
                    }
                }
                
                if(!isFound)
                {
                    string title = posterDataTable.Rows[i][0].ToString();
                    posterDataTable.Rows[i]["Ціна"] = 0;
                }
            }

            GC.Collect();
        }
        #endregion
        #region Add Products
        private void buttonAddUpdateWIX_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < posterDataTable.Rows.Count; i++)
            {
                counterLabel.Text = i.ToString() + "/" + posterDataTable.Rows.Count;
                int productCount = int.Parse(posterDataTable.Rows[i]["Кількість"].ToString());
                string hash = GenerateMD5(posterDataTable.Rows[i]["Назва"].ToString());
                DataRow[] rows = wixDataTable.Select("handleId like '%product_" + hash + "%'");
                if (rows.Length > 1)
                {
                    counterLabel.BackColor = Color.Red;
                    counterLabel.Text = "Check " + posterDataTable.Rows[i]["Назва"].ToString();
                    break;
                }
                else if (rows.Length > 0)
                {
                    rows[0]["visible"] = productCount > 0 ? "true" : "false";
                    rows[0]["inventory"] = Math.Max(0, productCount);
                    
                    float price;
                    if (float.TryParse(posterDataTable.Rows[i]["Ціна"].ToString(), out price) && price > 0)
                    {
                        rows[0]["price"] = price; 
                    }

                    richTextBoxUpdated.Text += rows[0]["name"] + "\n";
                }
                else
                {
                    if (int.Parse(posterDataTable.Rows[i]["Кількість"].ToString()) > 0)
                    {
                        DataRow row = wixDataTable.NewRow();
                        row["handleId"] = "product_" + GenerateMD5(posterDataTable.Rows[i]["Назва"].ToString());
                        row["fieldType"] = "Product";
                        row["name"] = posterDataTable.Rows[i]["Назва"];
                        row["collection"] = posterDataTable.Rows[i]["Тип"] + ";" + posterDataTable.Rows[i]["Контейнер"] + ";"
                            + posterDataTable.Rows[i]["Об'єм"] + ";" + posterDataTable.Rows[i]["Броварня"] + ";";
                        row["price"] = posterDataTable.Rows[i]["Ціна"];
                        row["visible"] = "true";
                        row["discountMode"] = "PERCENT";
                        row["discountValue"] = "0.0";
                        row["inventory"] = posterDataTable.Rows[i]["Кількість"];
                        row["weight"] = "0.0";
                        row["additionalInfoTitle1"] = "Стиль";
                        row["additionalInfoTitle2"] = "Броварня";
                        row["additionalInfoDescription2"] = "<p>" + posterDataTable.Rows[i]["Броварня"] + "</p>";
                        row["additionalInfoTitle3"] = "Алкоголь";
                        row["additionalInfoTitle4"] = "Гіркота";
                        row["additionalInfoTitle5"] = "Країна";
                        row["additionalInfoTitle6"] = "Untappd";

                        wixDataTable.Rows.Add(row);

                        richTextBoxAdded.Text += row["name"] + "\n";
                    }
                }
            }
        }
        #endregion
        #region Update IDs
        private void buttonUpdateWIXID_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < wixDataTable.Rows.Count; i++)
            {
                wixDataTable.Rows[i]["handleId"] = "product_" + GenerateMD5(wixDataTable.Rows[i]["name"].ToString());
            }
        }
        #endregion
        #region Helpers
        string OpenCSVFile()
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.DefaultExt = "*.csv";
            openFileDialog.Title = "Open File";
            openFileDialog.Filter = "|*.csv";
            if (openFileDialog.ShowDialog() != DialogResult.OK)
            {
                return String.Empty;
            }

            return System.IO.File.ReadAllText(openFileDialog.FileName);
        }
        DataTable ParseCSV(string csvText)
        {
            {
                int index = 0;
                while (index < csvText.Length && index > -1)
                {
                    index = csvText.IndexOf("\n", index + Math.Min(7, Math.Max(csvText.Length - index, 1)));
                    if (index < csvText.Length - 1 && index > -1 && csvText.Substring(index + 1, Math.Min(7, csvText.Length - index)) != "product")
                    {
                        string tmp = csvText.Substring(index, 1);
                        csvText = csvText.Remove(index, 1);
                        index -= 7;
                    }
                }
            }

            //{
            //    int index = 0;
            //    while (index < csvText.Length && index > -1)
            //    {
            //        index = csvText.IndexOf("<p>", index);
            //        if (index < 0)
            //        {
            //            break;
            //        }
            //        int endIndex = csvText.IndexOf("</p>", index);
            //        while (index < endIndex)
            //        {
            //            index = csvText.IndexOf(",", index);
            //            if (index >= endIndex)
            //            {
            //                break;
            //            }
            //            csvText = csvText.Remove(index, 1);
            //            csvText = csvText.Insert(index, @"\*");
            //        }
            //        index = endIndex;
            //    }
            //}
            DataTable dataTable = new DataTable();

            string[] lines = csvText.Split('\n');
            if (lines.Length > 0)
            {
                //first line to create header
                string firstLine = lines[0];
                string[] headerLabels = firstLine.Split(',');
                foreach (string headerWord in headerLabels)
                {
                    dataTable.Columns.Add(new DataColumn(headerWord));
                }
                //For Data
                for (int i = 1; i < lines.Length - 1; i++)
                {
                    string[] dataWords = new string[headerLabels.Length];
                    StringBuilder stringBuilder = new StringBuilder();
                    int dataWordsIndex = 0;
                    bool skipComa = false;
                    string skipPhrase = string.Empty;

                    for (int j = 0; j < lines[i].Length; j++)
                    {
                        char symbol = lines[i][j];
                        if(symbol == '"' && lines[i][j + 1] == '"')
                        {
                            stringBuilder.Append('"');
                            stringBuilder.Append('"');
                            j++;
                            continue;
                        }

                        if (!skipComa)
                        {
                            if (symbol == '"')
                            {
                                skipPhrase = '"'.ToString();
                            }
                            else if (j < lines[i].Length - 2 && symbol == '<' && lines[i][j + 1] == 'p' && lines[i][j + 2] == '>')
                            {
                                skipPhrase = "</p>";
                            }
                            skipComa = !string.IsNullOrEmpty(skipPhrase);
                        }
                        else
                        {
                            if (j < lines[i].Length - skipPhrase.Length)
                            {
                                skipComa = false;
                                for (int k = 0; k < skipPhrase.Length; k++)
                                {
                                    if (lines[i][j + k] != skipPhrase[k])
                                    {
                                        skipComa = true;
                                        break;
                                    }
                                }
                                if(!skipComa)
                                {
                                    skipPhrase = string.Empty;
                                }
                            }
                            else
                            {
                                skipPhrase = string.Empty;
                                skipComa = false;
                            }
                        }

                        if (!skipComa)
                        {
                            if (symbol == ',')
                            {
                                dataWords[dataWordsIndex++] = stringBuilder.ToString();
                                stringBuilder.Clear();
                                continue;
                            }
                        }
                        stringBuilder.Append(symbol);
                    }

                    //string[] dataWords = lines[i].Split(',');
                    DataRow dr = dataTable.NewRow();
                    int columnIndex = 0;
                    foreach (string headerWord in headerLabels)
                    {
                        dr[headerWord] = dataWords[columnIndex++];
                    }
                    dataTable.Rows.Add(dr);
                }
            }

            return dataTable;
        }
        bool LoadXLSFile(out int firstColumn, out int firstRow)
        {
            firstColumn = -1;
            firstRow = -1;

            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.DefaultExt = "*.xls;*.xlsx";
            openFileDialog.Title = "Open File";
            openFileDialog.Filter = "файл Excel|*.xls";
            if (openFileDialog.ShowDialog() != DialogResult.OK)
            {
                return false;
            }

            if (workBook != null)
            {
                workBook.Close(false, Type.Missing, Type.Missing);
            }
            workBook = workExcel.Workbooks.Open(openFileDialog.FileName);
            workSheet = (Excel.Worksheet)workBook.Sheets[1];

            Excel.Range lastCell = workSheet.Cells.SpecialCells(Excel.XlCellType.xlCellTypeLastCell);

            for (int i = 0; i < lastCell.Row; i++)
            {
                for (int j = 0; j < lastCell.Column; j++)
                {
                    if (workSheet.Cells[i + 1, j + 1].Text.ToString() != "")
                    {
                        firstRow = i + 1;
                        firstColumn = j + 1;
                        break;
                    }
                }

                if (firstRow != -1)
                {
                    break;
                }
            }

            return true;
        }
        PosterInfo ParsePosterName(string inName, string inBrewery)
        {
            string name = inName;
            PosterInfo posterName = new PosterInfo();
            string[] canNames = { " ж/б", " з/б", " банка", " бляшанка" };

            if (name.Split(' ').Length < 3)
            {
                posterName.Name = string.Empty;
                return posterName;
            }

            bool isCan = false;
            for (int i = 0; i < canNames.Length; i++)
            {
                if (name.Contains(canNames[i]))
                {
                    isCan = true;
                    name = name.Remove(name.IndexOf(canNames[i]), canNames[i].Length);
                    break;
                }
            }

            posterName.Container = isCan ? "Банка" : "Пляшка";
            posterName.Type = name.Split(' ')[0];
            name = name.Remove(0, posterName.Type.Length + 1);

            float volume;
            string volumeStr = name.Split(' ')[name.Split(' ').Length - 1];
            volumeStr = volumeStr.Replace('.', ',');
            if (float.TryParse(volumeStr, out volume))
            {
                volumeStr = name.Split(' ')[name.Split(' ').Length - 1];
                name = name.Remove(name.IndexOf(volumeStr) - 1, volumeStr.Length + 1);

                posterName.Volume = volumeStr.Replace(',', '.');
            }
            else
            {
                posterName.Volume = "0.33";
            }

            string[] breweryParts = inBrewery.Split(' ');
            for (int i = 0; i < breweryParts.Length; i++)
            {
                if (name.Contains(breweryParts[i]))
                {
                    name = name.Remove(name.IndexOf(breweryParts[i]), breweryParts[i].Length);
                }
            }

            posterName.Name = name + " " + (isCan ? posterName.Container + " " : "") + posterName.Volume;
            int index = 0;
            while(index > -1 && index < posterName.Name.Length)
            {
                index = posterName.Name.IndexOf('"', index);
                if (index > -1)
                {
                    if (index == posterName.Name.Length - 1)
                    {
                        posterName.Name = posterName.Name += '"';
                    }
                    else if (posterName.Name[index + 1] != '"')
                    {
                        posterName.Name = posterName.Name.Insert(index + 1, "\"");
                        index++;
                    }
                    index++;
                }
            }
            index = posterName.Name.IndexOf(",");
            if (index > -1 && index < posterName.Name.Length)
            {
                posterName.Name = '"' + posterName.Name + '"';
            }

            GC.Collect();
            return posterName;
        }

        string GenerateMD5(string name)
        {
            var md5 = MD5.Create();
            var hash = md5.ComputeHash(System.Text.Encoding.UTF8.GetBytes(name));
            char[] hashArray = Convert.ToBase64String(hash).ToCharArray();
            StringBuilder sb = new StringBuilder();
            foreach (char symbol in hashArray)
            {
                if(Char.IsDigit(symbol) || Char.IsLetter(symbol))
                {
                    sb.Append(symbol);
                }
            }
            return sb.ToString();
        }
        #endregion
        #region Save Wix CSV
        private void buttonSaveWIX_Click(object sender, EventArgs e)
        {
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.DefaultExt = "*.csv";
            saveFileDialog.Title = "Save File";
            saveFileDialog.Filter = "|*.csv";
            if (saveFileDialog.ShowDialog() != DialogResult.OK)
            {
                return;
            }

            StringBuilder sb = new StringBuilder();
            foreach (var column in wixDataTable.Columns)
            {
                sb.Append(column);
                sb.Append(",");
            }
            sb.Remove(sb.Length - 1, 1);
            sb.Append("\n");

            for (int j = 0; j < wixDataTable.Rows.Count; j++)
            {
                for(int i=0; i < wixDataTable.Columns.Count - 1; i++)
                {
                    sb.Append(wixDataTable.Rows[j][wixDataTable.Columns[i]]);
                    sb.Append(",");
                }
                sb.Append(wixDataTable.Rows[j][wixDataTable.Columns[wixDataTable.Columns.Count - 1]]);
                sb.Append("\n");
            }

            System.IO.File.WriteAllText(saveFileDialog.FileName, sb.ToString());
        }
        #endregion
        #region Update WIX Inventory
        private void buttonUpdateWIXInventory_Click(object sender, EventArgs e)
        {
            string[] countTableNames = { "Залишок", "Залишки" };
            int firstColumn;
            int firstRow;
            LoadXLSFile(out firstColumn, out firstRow);

            int countTableColumn = -1;
            Excel.Range lastCell = workSheet.Cells.SpecialCells(Excel.XlCellType.xlCellTypeLastCell);
            for (int j = firstColumn; j < lastCell.Column; j++)
            {
                for (int i = 0; i < countTableNames.Length; i++)
                {
                    if (workSheet.Cells[firstRow, j].Text.ToString() == countTableNames[i])
                    {
                        countTableColumn = j;
                        break;
                    }
                }
                if (countTableColumn > -1)
                {
                    break;
                }
            }

            List<PosterInfo> posterInfos = new List<PosterInfo>();
            for (int i = firstRow; i < lastCell.Row; i++)
            {
                counterLabel.Text = i.ToString() + "/" + lastCell.Row;

                float count;
                string tmp = workSheet.Cells[i + 1, countTableColumn].Text.ToString();
                if (float.TryParse(workSheet.Cells[i + 1, countTableColumn].Text.ToString(), out count))
                {
                    PosterInfo posterInfo = ParsePosterName(workSheet.Cells[i + 1, firstColumn].Text.ToString(), "");
                    posterInfo.Count = Math.Max(0, (int)count);
                    if (!string.IsNullOrEmpty(posterInfo.Name))
                    {
                        posterInfos.Add(posterInfo);
                    }
                }
            }

            DataTable dataTable = ParseCSV(OpenCSVFile());
            richTextBoxUpdated.Clear();

            for (int i = 0; i < posterInfos.Count; i++)
            {
                counterLabel.Text = i.ToString() + "/" + posterInfos.Count;
                string hash = GenerateMD5(posterInfos[i].Name);
                DataRow[] rows = dataTable.Select("handleId like '%product_" + hash + "%'");

                if (rows.Length > 0)
                {
                    int currentCount = -1;
                    if (!int.TryParse(rows[0]["inventory"].ToString(), out currentCount) || currentCount != (int)posterInfos[i].Count)
                    {
                        rows[0]["inventory"] = (int)posterInfos[i].Count;
                        richTextBoxUpdated.Text += posterInfos[i].Name + "\n";
                    }
                }
            }

            wixDataTable = dataTable;

            dataGridViewWIX.DataSource = dataTable;

            buttonSaveWIX_Click(null, null);
        }
        #endregion
        #region Update Collections
        private void button1_Click(object sender, EventArgs e)
        {
            //Стиль (IPA - American)
            //Тип (Пиво,Сидр)
            //Об'єм(0.33)
            //Алкоголь(0.5 % -5 % ABV)
            //Гіркота(0 - 20 IBU)
            //Пивоварня(Stone Brewing)
            //Тара(Пляшка)
            //Країна та регіон(Німеччина)
            //Додатки:
            //Untappd(Нелегально(4.5 та більше))

            string[] availableTypes = { "Пиво", "Сидр", "Мед", "Перрі", "Комбуча" };
            string[] possibleAditionals = { "Чисте", "Фрукти/ягоди", "Горіхи", "Перець", "Лактоза", "Спеції" };            

            Collections collections = new Collections();
            
            List<string> existedCollections = new List<string>();

            for (int i = 0; i < wixDataTable.Rows.Count; i++)
            {
                string tmp = wixDataTable.Rows[i]["collection"].ToString();
                if (tmp.Length > 0 && tmp[0] != ';' && tmp[0] != ' ' && tmp[0] != '0' && tmp[1] != '.')
                {
                    existedCollections.Add(wixDataTable.Rows[i]["collection"].ToString().ToLower());
                }
            }

            richTextBoxAdded.Text += "Нові колекції : \n";

            for (int i = 0; i < wixDataTable.Rows.Count; i++)
            {
                string type;
                string[] breweries;
                string country;
                string style;
                string style2;
                string ibu;
                string abv;
                List<string> additionals;
                string untappd;
                string volume;
                string container;

                string currentCollections = wixDataTable.Rows[i]["collection"].ToString().ToLower();

                style = wixDataTable.Rows[i]["additionalInfoDescription1"].ToString();
                StringBuilder stringBuilder = new StringBuilder();
                bool isFound = false;
                for (int j = 0; j < style.Length - 4; j++)
                {
                    if (!isFound)
                    {
                        if (style[j] == '<' && style[j + 1] == 'p' && style[j + 2] == '>')
                        {
                            isFound = true;
                            j += 2;
                        }
                    }
                    else
                    {
                        if (style[j] == '<' && style[j + 1] == '/' && style[j + 2] == 'p' && style[j + 3] == '>')
                        {
                            break;
                        }
                        stringBuilder.Append(style[j]);
                    }
                }
                style = stringBuilder.ToString();

                type = "Пиво";
                for (int j = 0; j < availableTypes.Length; j++)
                {
                    if (currentCollections.Contains(availableTypes[j].ToLower()))
                    {
                        type = availableTypes[j];
                        break;
                    }
                }

                string name = wixDataTable.Rows[i]["name"].ToString();
                volume = name.Split(' ')[name.Split(' ').Length - 1].Replace("\"", "");

                abv = wixDataTable.Rows[i]["additionalInfoDescription3"].ToString();
                stringBuilder.Clear();
                isFound = false;
                bool isDigitFound = false;
                for (int j = 0; j < abv.Length; j++)
                {
                    if (Char.IsDigit(abv[j]))
                    {
                        stringBuilder.Append(abv[j]);
                        isDigitFound = true;
                    }
                    else if ((abv[j] == '.' || abv[j] == ',') && !isFound)
                    {
                        stringBuilder.Append(abv[j]);
                        isFound = true;
                    }
                    else if (isDigitFound)
                    {
                        break;
                    }
                }

                float previousValue = 0;
                float parsedFloat = 0;

                if (float.TryParse(stringBuilder.ToString().Replace('.', ','), out parsedFloat))
                {
                    for (int j = 0; j < collections.ABVCollection.Count; j++)
                    {
                        if (previousValue < parsedFloat && collections.ABVCollection[j].Item1 >= parsedFloat)
                        {
                            abv = collections.ABVCollection[j].Item2;
                            break;
                        }
                        previousValue = collections.ABVCollection[j].Item1;
                    }
                }
                else
                {
                    abv = collections.ABVCollection[0].Item2;
                }


                ibu = wixDataTable.Rows[i]["additionalInfoDescription4"].ToString().Replace("<p>", "").Replace("</p>", "");
                stringBuilder.Clear();
                isDigitFound = false;
                for (int j = 0; j < ibu.Length; j++)
                {
                    if (Char.IsDigit(ibu[j]))
                    {
                        stringBuilder.Append(ibu[j]);
                        isDigitFound = true;
                    }
                    else if (isDigitFound)
                    {
                        break;
                    }
                }

                int previousIBU = 0;
                int parsedInt = 0;
                previousValue = 0;
                if (int.TryParse(stringBuilder.ToString(), out parsedInt))
                {
                    for (int j = 0; j < collections.IBUCollection.Count; j++)
                    {
                        if (previousValue < parsedInt && collections.IBUCollection[j].Item1 >= parsedInt)
                        {
                            ibu = collections.IBUCollection[j].Item2;
                            break;
                        }
                        previousIBU = collections.IBUCollection[j].Item1;
                    }
                }
                else
                {
                    ibu = collections.IBUCollection[0].Item2;
                }

                breweries = wixDataTable.Rows[i]["additionalInfoDescription2"].ToString().Replace("\"", "").Replace("<p>", "").Replace("</p>", "").Split('|');
                for (int j = 0; j < breweries.Length; j++)
                {
                    if (breweries[j][0] == ' ')
                    {
                        breweries[j] = breweries[j].Remove(0, 1);
                    }
                    if (breweries[j][breweries[j].Length - 1] == ' ')
                    {
                        breweries[j] = breweries[j].Remove(breweries[j].Length - 1, 1);
                    }
                }

                if (name.ToLower().Contains("банка"))
                {
                    container = "Банка";
                }
                else
                {
                    container = "Пляшка";
                }

                country = wixDataTable.Rows[i]["additionalInfoDescription5"].ToString().Replace("\"", "").Replace("<p>", "").Replace("</p>", "");

                untappd = wixDataTable.Rows[i]["additionalInfoDescription6"].ToString();
                int index = untappd.IndexOf("</a>");
                stringBuilder.Clear();

                isFound = false;
                for (int j = index - 1; j > 0; j--)
                {
                    if (Char.IsDigit(untappd[j]))
                    {
                        stringBuilder.Append(untappd[j]);
                    }
                    else if (!isFound && (untappd[j] == '.' || untappd[j] == ','))
                    {
                        stringBuilder.Append(untappd[j]);
                        isFound = true;
                    }
                    else
                    {
                        break;
                    }
                }

                previousValue = 0;
                parsedFloat = 0;
                untappd = stringBuilder.ToString();
                stringBuilder.Clear();
                for (int j = untappd.Length - 1; j >= 0; j--)
                {
                    if (untappd[j] == '.')
                    {
                        stringBuilder.Append(',');
                    }
                    else
                    {
                        stringBuilder.Append(untappd[j]);
                    }
                }

                if (float.TryParse(stringBuilder.ToString(), out parsedFloat))
                {
                    for (int j = 0; j < collections.untappdCollection.Count; j++)
                    {
                        if (previousValue < parsedFloat && collections.untappdCollection[j].Item1 >= parsedFloat)
                        {
                            untappd = collections.untappdCollection[j].Item2;
                            break;
                        }
                        previousValue = collections.untappdCollection[j].Item1;
                    }
                }
                else
                {
                    untappd = collections.untappdCollection[0].Item2;
                }

                additionals = new List<string>();
                for (int j = 0; j < possibleAditionals.Length; j++)
                {
                    if (currentCollections.Contains(possibleAditionals[j].ToLower()))
                    {
                        additionals.Add(possibleAditionals[j]);
                    }
                }

                stringBuilder.Clear();
                stringBuilder.Append("\"");
                stringBuilder.Append(style);
                stringBuilder.Append(";");
                stringBuilder.Append(type);
                stringBuilder.Append(";");
                stringBuilder.Append(volume);
                stringBuilder.Append(";");
                stringBuilder.Append(abv);
                stringBuilder.Append(";");
                stringBuilder.Append(ibu);
                stringBuilder.Append(";");
                for (int j = 0; j < breweries.Length; j++)
                {
                    stringBuilder.Append(breweries[j]);
                    stringBuilder.Append(";");
                }
                stringBuilder.Append(container);
                stringBuilder.Append(";");

                stringBuilder.Append(untappd);
                stringBuilder.Append(";");

                stringBuilder.Append(country);
                stringBuilder.Append(";");

                if (country.Contains("Україна"))
                {
                    stringBuilder.Append("Україна;");
                }

                string styleCommon = ParseStyleToHumanStyles(style);
                if(string.IsNullOrEmpty(styleCommon))
                {
                    richTextBoxUpdated.Text += "Can't parse style : " + style + "\n";
                }
                stringBuilder.Append(styleCommon);
                stringBuilder.Append(";");


                stringBuilder.Remove(stringBuilder.Length - 1, 1);
                stringBuilder.Append("\"");

                wixDataTable.Rows[i]["collection"] = stringBuilder.ToString().Replace("&nbsp;", "");

                bool styleIsFound = false;
                bool typeIsFound = false;
                bool volumeIsFound = false;
                bool breweryIsFound = false;
                bool conteinerIsFound = false;
                bool countryIsFound = false;

                foreach (var existedCollection in existedCollections)
                {

                    styleIsFound |= existedCollection.Contains(style.ToLower());
                    typeIsFound |= existedCollection.Contains(type.ToLower());
                    volumeIsFound |= existedCollection.Contains(volume.ToLower());
                    breweryIsFound |= existedCollection.Contains(breweries[0].ToLower());
                    conteinerIsFound |= existedCollection.Contains(container.ToLower());
                    countryIsFound |= existedCollection.Contains(country.ToLower());
                }

                if (!styleIsFound || !typeIsFound || !typeIsFound || !breweryIsFound || !conteinerIsFound || !countryIsFound)
                {
                    richTextBoxAdded.Text += (!styleIsFound ? style : "") + " ; "
                    + (!typeIsFound ? type : "") + " ; "
                    + (!typeIsFound ? volume : "") + " ; "
                    + (!breweryIsFound ? breweries[0] : "") + " ; "
                    + (!conteinerIsFound ? container : "") + " ; "
                    + (!countryIsFound ? country : "") + "\n";
                }
            }
        }
        #endregion

        string ParseStyleToHumanStyles(string style)
        {
            Collections collections = new Collections();
            StringBuilder sb = new StringBuilder();

            for(int i=0; i< collections.RedAndAmberStyles.Length; i++)
            {
                if (style.ToLower().Contains(collections.RedAndAmberStyles[i].ToLower()))
                {
                    sb.Append("Red / Amber;");
                    break;
                }
            }

            for (int i = 0; i < collections.BrownAndDarkStyles.Length; i++)
            {
                if (style.ToLower().Contains(collections.BrownAndDarkStyles[i].ToLower()))
                {
                    sb.Append("Brown / Dark;");
                    break;
                }
            }

            for (int i = 0; i < collections.IPAStyles.Length; i++)
            {
                if (style.ToLower().Contains(collections.IPAStyles[i].ToLower()))
                {
                    sb.Append("IPA;");
                    break;
                }
            }

            for (int i = 0; i < collections.PaleAndBlondStyles.Length; i++)
            {
                if (style.ToLower().Contains(collections.PaleAndBlondStyles[i].ToLower()))
                {
                    sb.Append("Pale / Blond;");
                    break;
                }
            }

            for (int i = 0; i < collections.BelgianLikeStyles.Length; i++)
            {
                if (style.ToLower().Contains(collections.BelgianLikeStyles[i].ToLower()))
                {
                    sb.Append("Belgian;");
                    break;
                }
            }

            for (int i = 0; i < collections.StoutAndPorterStyles.Length; i++)
            {
                if (style.ToLower().Contains(collections.StoutAndPorterStyles[i].ToLower()))
                {
                    sb.Append("Stout / Porter;");
                    break;
                }
            }

            for (int i = 0; i < collections.SourWildFarmhouseStyles.Length; i++)
            {
                if (style.ToLower().Contains(collections.SourWildFarmhouseStyles[i].ToLower()))
                {
                    sb.Append("Sour / Wild / Season;");
                    break;
                }
            }

            for (int i = 0; i < collections.PilsnerLagerStyles.Length; i++)
            {
                if (style.ToLower().Contains(collections.PilsnerLagerStyles[i].ToLower()))
                {
                    sb.Append("Pilsner / Lager;");
                    break;
                }
            }

            for (int i = 0; i < collections.WithAdditionalsStyles.Length; i++)
            {
                if (style.ToLower().Contains(collections.WithAdditionalsStyles[i].ToLower()))
                {
                    sb.Append("Fruit / Spice / Smoked;");
                    break;
                }
            }

            for (int i = 0; i < collections.WheatWitStyles.Length; i++)
            {
                if (style.ToLower().Contains(collections.WheatWitStyles[i].ToLower()))
                {
                    sb.Append("Wheat / Wit / Weizen;");
                    break;
                }
            }

            for (int i = 0; i < collections.StrongStyles.Length; i++)
            {
                if (style.ToLower().Contains(collections.StrongStyles[i].ToLower()))
                {
                    sb.Append("Strong;");
                    break;
                }
            }

            for (int i = 0; i < collections.CiderMeadStyles.Length; i++)
            {
                if (style.ToLower().Contains(collections.CiderMeadStyles[i].ToLower()))
                {
                    sb.Append("Cider / Mead;");
                    break;
                }
            }

            for (int i = 0; i < collections.NonAlcoholic.Length; i++)
            {
                if (style.ToLower().Contains(collections.NonAlcoholic[i].ToLower()))
                {
                    sb.Append("Non-Alcoholic;");
                    break;
                }
            }


            return sb.ToString();
        }

        private void UpdatePhotosButton_Click(object sender, EventArgs e)
        {
            string[] photosURL =
            {
                "75be58_1e6f529af313468ab7445c096ab03f30~mv2.png",
                "75be58_181795fc1d2645a6a08f273179bf5e03~mv2.png",
                "75be58_58963014994744f39191e04aca2be10e~mv2.png",
                "75be58_0e71b55113a8452ca9bb74bbed2b9061~mv2.png",
                "75be58_ce3400a7dec54b9381a9080f7056ec69~mv2.png",
                "75be58_26ef1dda8a7448f29d06d603f4f8f618~mv2.png"
            };

            Random rand = new Random();
            for (int i = 0; i < wixDataTable.Rows.Count; i++)
            {
                bool isRandomPhoto = false;
                string currentUrl = wixDataTable.Rows[i]["productImageUrl"].ToString();
                for (int j = 0; j < photosURL.Length; j++)
                {
                    isRandomPhoto |= currentUrl == photosURL[j];
                }

                if (string.IsNullOrEmpty(wixDataTable.Rows[i]["productImageUrl"].ToString()) || isRandomPhoto)
                {
                    wixDataTable.Rows[i]["productImageUrl"] = photosURL[rand.Next(0, photosURL.Length)];
                    richTextBoxUpdated.Text += wixDataTable.Rows[i]["name"] + "\n";
                }
            }
        }
    }
}
